package com.thortful.kotlindemo.part4

data class FlatObject(
    val name: String,
    val age: Int
)