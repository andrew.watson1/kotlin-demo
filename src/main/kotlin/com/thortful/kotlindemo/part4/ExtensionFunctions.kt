package com.thortful.kotlindemo.part4

import com.thortful.kotlindemo.part3.Outer

object ExtensionFunctions {
    fun Outer.toFlatObject(): FlatObject {
        return FlatObject(name = this.inner.name, age = this.inner.age)
    }

}