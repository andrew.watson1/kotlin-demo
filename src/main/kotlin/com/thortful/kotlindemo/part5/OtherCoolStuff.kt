package com.thortful.kotlindemo.part5

object OtherCoolStuff {
    fun letDemo() {
        val let = "My name is andy".let {
            println(it)
            println("The length is ${it.length}")
            it.length
        }
    }

    fun applyDemo(name: String, age: Int, address: String?, email: String?) {
        BuilderObj
            .builder()
            .name(name)
            .age(age)
            .apply {
                if (address != null) address(address)
                if (email != null) email(email)
            }
            .build()
    }
}