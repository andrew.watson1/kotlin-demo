package com.thortful.kotlindemo.part5

class ThrowAnException {
    fun thisThrowsAnException() {
        throw DemoException("I don't need to be checked!")
    }
}