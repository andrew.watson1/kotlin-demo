package com.thortful.kotlindemo.part5

class DemoException(override val message: String): Exception(message)