package com.thortful.kotlindemo.part1

import com.thortful.kotlindemo.part3.Inner
import com.thortful.kotlindemo.part3.Outer

class Basics {
    fun myFirstFun() {
        val valsAreImmutable = 5
        var varsAreNot = 5

        val string1 = "This is a string"
        val string2 = "So is this"
        if (string1 == string2) {
            println("Strings are equal")
        } else {
            println("Strings are different")
        }

        when(valsAreImmutable) {
            5 -> println("It's 5!")
            6 -> println("No it's 6!")
            else -> println("It's something else")
        }

        val name = "Andy"
        val interpolated = "my name is $name"
        val outer = Outer(Inner(name, 1))
        val interpolatedWithRawString = """{
            |"name": "${outer.inner.name}",
            |"age": 28
            |}""".trimMargin()

        val noMoreTernaryOperator = if (name == "Andy") "It's me!" else "Someone else!"
    }
}