package com.thortful.kotlindemo.part2

import com.thortful.kotlindemo.part3.Outer

object SmartCastingAndTypeInference {
    fun dontNeedAnExplicitReturnType(name: String) = "Hello $name!"

    fun declaringAList(){
        val list = listOf("This", "is", "better", null, null)
        val lens = list.filterNotNull().map(String::length)
    }

    fun smartCastingFromNullable(outer: Outer?) {
        if (outer != null) {
            val x = outer.inner.age
        }
    }
}