package com.thortful.kotlindemo.part2

object CoolFunctionStuff {

    fun thisDoesNeedABody(x: Int, y: Int): Int {
        println("Are values are $x and $y")
        val res = x * y
        print("The result is $res")
        return res
    }

    fun doesNotNeedABody(x: Int, y: Int) = x + y

    fun functionWithDefaults(x: Int = 5, y: Int = 5) = x * y


}