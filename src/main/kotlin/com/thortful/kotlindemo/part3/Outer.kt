package com.thortful.kotlindemo.part3

data class Outer(
    val inner: Inner
)

data class Inner(
    val name: String,
    val age: Int
)