package com.thortful.kotlindemo.part3

class CoolNullStuff {
    fun nullFunction(nonNullable: String, nullable: String?) {
        val x = nullable ?: "It was null!"
    }

    fun functionWithNestedNullableObject(outer: Outer?) {
        val age = outer?.inner?.age
    }
}