package com.thortful.kotlindemo.part3

class CoolDataClassStuff {
    fun coolStuff() {
        val andy = Outer(inner = Inner(age = 28, name = "Andy"))
        val andyCopy = andy.copy()
        val andyButOlder = andy.copy(inner = andy.inner.copy(age = 29))
    }
}