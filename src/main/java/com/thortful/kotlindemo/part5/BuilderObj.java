package com.thortful.kotlindemo.part5;

import lombok.Builder;

@Builder
public class BuilderObj {
    String name;
    Integer age;
    String address;
    String email;
}
