package com.thortful.kotlindemo.part5;

public class UsingLombok {
    public void lombokDemo(String name, Integer age, String email, String address) {
        BuilderObj.BuilderObjBuilder builder = BuilderObj
                .builder()
                .name(name)
                .age(age);

        if (email != null) {
            builder.email(email);
        }
        if (address != null) {
            builder.address(address);
        }
        BuilderObj realObj = builder.build();
    }
}
