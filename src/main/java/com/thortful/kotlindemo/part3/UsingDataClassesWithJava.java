package com.thortful.kotlindemo.part3;

import com.thortful.kotlindemo.part3.Inner;
import com.thortful.kotlindemo.part3.Outer;

public class UsingDataClassesWithJava {
    public void dataClass() {
        Outer outer =  new Outer(new Inner("Andy", 28));
        outer.getInner().getAge();
        outer.getInner().getName();
    }
}
