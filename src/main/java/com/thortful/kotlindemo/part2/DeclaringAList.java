package com.thortful.kotlindemo.part2;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class DeclaringAList {
    public void declare() {
        List<String> list = Arrays.asList("This", "Is", "Cumbersome", null, null);
        List<Integer> averageNonNullLength = list.stream()
                .filter(Objects::nonNull)
                .map(x -> x.length())
                .collect(Collectors.toList());
    }
}
