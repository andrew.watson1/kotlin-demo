import com.thortful.kotlindemo.part2.CoolFunctionStuff.functionWithDefaults
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class CoolFunctionStuffTest {

    @Test
    fun `Should use defaults if not provided`() {
        assertEquals(25, functionWithDefaults())
    }

    @Test
    fun `Should use args if provided`() {
        assertEquals(50, functionWithDefaults(x = 10))
        assertEquals(30, functionWithDefaults(y = 6))
        assertEquals(70, functionWithDefaults(x = 10, y = 7))
    }
}