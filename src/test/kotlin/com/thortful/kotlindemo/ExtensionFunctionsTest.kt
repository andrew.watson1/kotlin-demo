package com.thortful.kotlindemo

import com.thortful.kotlindemo.part4.ExtensionFunctions.toFlatObject
import com.thortful.kotlindemo.part3.Inner
import com.thortful.kotlindemo.part3.Outer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ExtensionFunctionsTest {

    @Test
    fun `extension function test`() {
        val outer = Outer(Inner(name = "Andy", age = 28))
        val flatObject = outer.toFlatObject()
        assertEquals(outer.inner.name, flatObject.name)
        assertEquals(outer.inner.age, flatObject.age)
    }
}